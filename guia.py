from database import Database
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class ventana():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("ventana_princ.glade")
        self.ventana = self.builder.get_object("ventana_princ")
        self.ventana.show_all()
        self.ventana.maximize()
        self.ventana.connect("destroy", Gtk.main_quit)
        self.boton_agregar = self.builder.get_object("boton_agregar")
        self.boton_agregar.connect("clicked", self.agregar_compuesto)
        self.boton_borrar = self.builder.get_object("boton_borrar")
        self.boton_borrar.connect("clicked", self.borrar_seleccion)
        self.boton_editar = self.builder.get_object("boton_editar")
        self.boton_editar.connect("clicked", self.editar_seleccion)

        #copiado
        self.listmodel = Gtk.ListStore(str, str, str)
        self.table = self.builder.get_object("treeview")
        self.table.set_model(model=self.listmodel)

        cell = Gtk.CellRendererText()
        titles = ["nombre", "categoria", "composicion quimica"]
        for i in range(len(titles)):
            col = Gtk.TreeViewColumn(titles[i], cell, text=i)
            self.table.append_column(col)

        self.mostrar_tabla()

    def obtener_seleccion(self):
        model, it = self.table.get_selection().get_selected()
        if it is None:
            return
        nombre = model.get_value(it, 0)
        categoria = model.get_value(it, 1)
        composicion = model.get_value(it, 2)

        return (nombre, categoria, composicion)

    def mostrar_tabla (self):
        #copiado
        if len(self.listmodel) > 0:
            for i in range(len(self.listmodel)):
                iter = self.listmodel.get_iter(0)
                self.listmodel.remove(iter)

        data = Database.load("elementos.json")
        for key in list(data.keys()):
            for item in data[key]:
                self.listmodel.append([item[0], key, item[1]])

    def agregar_compuesto(self, boton=None):
        def actualizar(boton=None):
            self.mostrar_tabla()

        ventana = ventana_agregar()
        ventana.ventana.connect("destroy", actualizar)

    def borrar_seleccion(self, boton=None):
        nombre, categoria, composicion = self.obtener_seleccion()

        if nombre == "":
            return

        mensaje = Gtk.MessageDialog(type=Gtk.MessageType.WARNING, buttons=Gtk.ButtonsType.YES_NO)
        mensaje.format_secondary_text("realmente quiere borrar " + nombre + " ?")

        def response(dialogo, apretado):
            dialogo.close()
            si = Gtk.ResponseType.YES

            if apretado == si:
                self.borrar_definitivamente(nombre, categoria, composicion)
                self.mostrar_tabla()

        mensaje.connect("response", response)
        mensaje.run()


    def borrar_definitivamente(self, nombre, categoria, composicion):
        data = Database.load("elementos.json")

        for i in range(len(data[categoria])):
            if data[categoria][i][0] == nombre and data[categoria][i][1] == composicion:
                del data[categoria][i]
                break

        Database.save("elementos.json", data)

    def editar_seleccion(self, boton=None):
        return


class ventana_agregar:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("agregar.glade")
        self.ventana = self.builder.get_object("ventana-Agrega")
        self.ventana.show_all()
        self.nombre = self.builder.get_object("nombre")
        self.composicion = self.builder.get_object("composicion")
        self.combobox = self.builder.get_object("combobox")
        self.btn_cancelar = self.builder.get_object("cancelar")
        self.btn_aceptar = self.builder.get_object("aceptar")
        self.btn_cancelar.connect("clicked", self.salir)
        self.btn_aceptar.connect("clicked", self.agregar)


    def salir(self, boton=None):
        self.ventana.close()


    def agregar(self, boton=None):
        nombre = self.nombre.get_text()
        categoria = self.combobox.get_model()[self.combobox.get_active()][0]
        composicion = self.composicion.get_text()

        if composicion == "" or self.combobox.get_active() == -1 or nombre == "":
            return

        elementos = Database.load("elementos.json")
        elementos[categoria].append([nombre, composicion])
        Database.save("elementos.json", elementos)
        self.ventana.close()
if __name__ == "__main__":
    w = ventana()
    Gtk.main()
